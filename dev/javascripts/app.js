//https://jquery.com/
@import "../assets/jquery/dist/jquery.min.js";

//http://underscorejs.org/
//@import "../assets/underscore/underscore-min.js";

//http://getbootstrap.com/
@import "../assets/bootstrap-sass/assets/javascripts/bootstrap.min.js";

//http://silviomoreto.github.io/bootstrap-select/
//@import "../assets/bootstrap-select/dist/js/bootstrap-select.min.js";

//https://github.com/seiyria/bootstrap-slider
//@import "../assets/seiyria-bootstrap-slider/dist/bootstrap-slider.min.js";

//https://github.com/dreyescat/bootstrap-rating
//@import "../assets/bootstrap-rating/bootstrap-rating.min.js";

//http://www.chartjs.org/docs/
//@import "../assets/Chart.js/Chart.min.js";

//https://github.com/eternicode/bootstrap-datepicker/blob/master/docs/index.rst
//@import "../assets/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js";
//@import "../assets/bootstrap-datepicker/dist/locales/bootstrap-datepicker.cs.min.js";

//https://github.com/nathan-muir/fontawesome-markers
//@import "../assets/fontawesome-markers/fontawesome-markers.min.js"

"use strict";

var $doc = $(document);
var $win = $(window);

$doc.ready(function(){
	
});

$win.resize(function(){

});

$doc.load(function(){

});

$win.load(function(){

});

(function(globals){
	globals.App = {
		Custom: {},
	};
}(this));